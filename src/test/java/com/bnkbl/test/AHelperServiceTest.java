package com.bnkbl.test;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class AHelperServiceTest {

  AHelperService subject = new AHelperService();

  @Test
  void shouldReturnTrueForValidUsername() {
    boolean result = subject.foo("AppleApple");
    assertThat(result).isFalse();
  }
}